// Calendar relate mutations
export const ADD_EVENT_DONE = 'ADD_EVENT_DONE';
export const ACTION_FAILED = 'ACTION_FAILED';
export const DELETE_EVENT_DONE = 'DELETE_EVENT_DONE';
export const GET_EVENTS_DONE = 'GET_EVENTS_DONE';
export const RESET_ACTION_ERROR = 'RESET_ACTION_ERROR';
export const UPDATE_EVENT_DONE = 'UPDATE_EVENT_DONE';

// Dialog related mutations
export const CLOSE_DIALOG = 'CLOSE_DIALOG';
export const OPEN_DIALOG = 'OPEN_DIALOG';
