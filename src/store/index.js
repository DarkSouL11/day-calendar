import Vue from 'vue';
import Vuex from 'vuex';

import dialog from './modules/dialog';
import calendar from './modules/calendar';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    dialog,
    calendar
  }
});

export default store;
