import Vue from 'vue';

import * as con from '../constants';
import eventsApi from '../../api';
import {
  EVENT_BACKGROUNDS,
  MINS_IN_DAY,
  MIN_TIME_INTERVAL
} from '../../utils/constants';
import { intervalLabel } from '../../utils/datetime';

const NUM_COLORS = EVENT_BACKGROUNDS.length;

const calendar = {
  state: {
    events: {},
    error: null
  },
  getters: {
    blueprint: function(state) {
      let blueprint = [];

      for(let i = 0; i <= MINS_IN_DAY; i += MIN_TIME_INTERVAL ) {
        blueprint.push({
          start: i,
          associatedEventId: null
        })
      }

      Object.keys(state.events).forEach(eventId => {
        const { startTime, endTime } = state.events[eventId];
        for(let i = startTime; i < endTime; i += MIN_TIME_INTERVAL) {
          blueprint[i / MIN_TIME_INTERVAL].associatedEventId = eventId;
        }
      });
      return blueprint;
    },
    getAssociatedEvent: function(state, { getAssociatedEventId }) {
      return function(rowIndex) {
        const eventId = getAssociatedEventId(rowIndex);
        if (!eventId) return false;
        return state.events[eventId];
      }
    },
    getAssociatedEventId: function(state, { blueprint }) {
      return function(rowIndex) {
        return blueprint[rowIndex].associatedEventId;
      }
    },
    getIntervalLabel: function(state, { getAssociatedEventId }) {
      return function(rowIndex) {
        const eventId = getAssociatedEventId(rowIndex);
        const { startTime, endTime } = state.events[eventId];
        return intervalLabel(startTime, endTime);
      }
    },
    /**
     * Helper for generate row background color.
     */
    getRowStyle: function(state, { getAssociatedEventId }) {
      return function(rowIndex) {
        const style = { background: '#ffffff' };
        const eventId = getAssociatedEventId(rowIndex);
        if (eventId) {
          style.background = EVENT_BACKGROUNDS[eventId % NUM_COLORS];
        }
        return style;
      }
    },
    /**
     * Indicates if the current row is the first row in the event time span.
     * Ex. If event is between `2:00 am` and `4:00 am` then the object in
     * blueprint which has `start =  120(i.e, the minutes representation of
     * 2:00 am)` will the be the start block.
     */
    isFirstRowInEvent: function(state, { getAssociatedEventId }) {
      return function(rowIndex) {
        const currRowEventId = getAssociatedEventId(rowIndex);
        if (!currRowEventId) return false;
        if (rowIndex === 0) return true;

        const prevRowEventId = getAssociatedEventId(rowIndex - 1);
        return (prevRowEventId !== currRowEventId);
      }
    },
    /**
     * Indicates if the current row is the last row in the event time span.
     * Ex. If event is between `2:00 am` and `4:00 am` then the object in
     * blueprint which has `start =  240(i.e, the minutes representation of
     * 4:00 am)` will the be the end block.
     */
    isLastRowInEvent: function(state, { blueprint, getAssociatedEventId }) {
      return function(rowIndex) {
        const currRowEventId = getAssociatedEventId(rowIndex);
        if (!currRowEventId) return false;
        if (rowIndex + 1 === blueprint.length) return true;

        const nextRowEventId = getAssociatedEventId(rowIndex + 1);
        return (nextRowEventId !== currRowEventId);
      }
    }
  },
  mutations: {
    [con.ADD_EVENT_DONE](state, data) {
      Vue.set(state.events, data.id, data.eventData);
    },
    [con.ACTION_FAILED](state, data) {
      state.error = data.message;
    },
    [con.DELETE_EVENT_DONE](state, data) {
      Vue.delete(state.events, data.id);
    },
    [con.GET_EVENTS_DONE](state, data) {
      state.events = data.eventsData;
    },
    [con.RESET_ACTION_ERROR](state) {
      state.error = null;
    },
    [con.UPDATE_EVENT_DONE](state, data) {
      state.events[data.id] = data.eventData;
    }
  },
  actions: {
    getEvents({ commit }) {
      eventsApi.configure();
      const eventsData = eventsApi.get();
      commit(con.GET_EVENTS_DONE, { eventsData });
    },
    addEvent({ commit }, { eventData }) {
      commit(con.RESET_ACTION_ERROR);
      try {
        const id = eventsApi.add(eventData);
        commit(con.ADD_EVENT_DONE, { id, eventData });
        commit(con.CLOSE_DIALOG);
      } catch (e) {
        commit(con.ACTION_FAILED, e);
      }
    },
    updateEvent({ commit }, { id, eventData }) {
      commit(con.RESET_ACTION_ERROR);
      try {
        eventsApi.update(id, eventData);
        commit(con.UPDATE_EVENT_DONE, { id, eventData });
        commit(con.CLOSE_DIALOG);
      } catch (e) {
        commit(con.ACTION_FAILED, e);
      }
    },
    deleteEvent({ commit }, { id }) {
      commit(con.RESET_ACTION_ERROR);
      try {
        eventsApi.delete(id);
        commit(con.DELETE_EVENT_DONE, { id });
        commit(con.CLOSE_DIALOG);
      } catch (e) {
        commit(con.ACTION_FAILED, e)
      }
    }
  }
}

export default calendar;