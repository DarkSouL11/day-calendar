import { CLOSE_DIALOG, OPEN_DIALOG } from '../constants';

const dialog = {
  state: {
    selectedRowIndex: null
  },
  mutations: {
    [CLOSE_DIALOG](state) {
      state.selectedRowIndex = null;
    },
    [OPEN_DIALOG](state, { selectedRowIndex }) {
      state.selectedRowIndex = selectedRowIndex;
    }
  }
}

export default dialog;
