import localStore from './localStore';
import { STORAGE_KEY } from '../utils/constants';

const d = new Date();
const EVENTS_KEY = `${d.getDate()}-${d.getMonth()}-${d.getFullYear()}`;

function configureEvents() {
  let lsData = localStore.getItem(STORAGE_KEY);
  if (!(lsData && lsData[EVENTS_KEY])) {
     lsData = {
       [EVENTS_KEY]: {},
       /**
        * Below key will be used to generate a unique id for every event that
        * will created be created, we increment this everytime a new event is
        * added and the resulting value will be used as `id` for the new event.
        * It is important to note that we will not decrement this value when an
        * event is removed/deleted.
        */
       eventsAdded: 0
     }
     localStore.setItem(STORAGE_KEY, lsData);
  }
}

function getNextId() {
  const lsData = localStore.getItem(STORAGE_KEY);
  lsData.eventsAdded += 1;
  localStore.setItem(STORAGE_KEY, lsData);
  return lsData.eventsAdded;
}

function getEvents() {
  return localStore.getItem(STORAGE_KEY)[EVENTS_KEY];
}

function setEvents(eventsData) {
  const lsData = localStore.getItem(STORAGE_KEY);
  lsData[EVENTS_KEY] = eventsData;
  localStore.setItem(STORAGE_KEY, lsData);
}

function addEvent(data) {
  const eventsData = getEvents();
  /**
   * Raises error if there is another event that conflicts with the start and
   * end times of new event. Practically one person can't be attending two
   * events at the same time, so it is restricted to create multiple events
   * which have their start and end times overlapping.
   */
  let isConflicted = false;
  Object.keys(eventsData).some(eventId => {
    const { startTime, endTime } = eventsData[eventId];
    isConflicted = !(data.endTime <= startTime || data.startTime >= endTime);
    return isConflicted;
  });

  if (isConflicted) {
    throw new Error('Event time conflicts with some other event');
  } else {
    const newEventId = getNextId();
    eventsData[newEventId] = data;
    setEvents(eventsData);
    return newEventId;
  }
}

function updateEvent(id, updatedEvent) {
  const eventsData = getEvents();
  const eventToUpdate = eventsData[id];
  if (eventToUpdate) {
    /**
     * Check to see if the updated event conflicts with any other event's time.
     */
    let isConflicted = false
    Object.keys(eventsData).some(eventId => {
      const { startTime, endTime } = eventsData[eventId];
      isConflicted = eventId !== id &&
        !(updatedEvent.endTime <= startTime || updatedEvent.startTime >= endTime);
      return isConflicted;
    });

    if (isConflicted) {
      throw new Error('Event time conflicts with some other event');
    } else {
      eventsData[id] = updatedEvent;
      setEvents(eventsData);
      return updatedEvent;
    }
  } else {
    throw new Error('No event found for updating');
  }
}

function deleteEvent(eventId) {
  const eventsData = getEvents();
  delete eventsData[eventId];
  setEvents(eventsData);
}

export default {
  configure: configureEvents,
  get: getEvents,
  add: addEvent,
  update: updateEvent,
  delete: deleteEvent
}