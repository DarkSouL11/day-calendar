const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
  'Oct', 'Nov', 'Dec'];

export function shortDate(d) {
  const date = new Date(d);
  return `${date.getDate()} ${MONTHS[date.getMonth()]}`;
}

export function meridianTime(timeInMin) {
  let hours = parseInt(timeInMin / 60, 10), minutes = (timeInMin % 60),
    meridian = 'AM';
  if (hours >= 12) {
    hours = hours - 12;
    meridian = 'PM'
  }

  if (hours === 0) {
    hours = 12
  }

  if (minutes < 10) {
    minutes = '0' + minutes;
  }

  return `${hours}:${minutes} ${meridian}`;
}

export function intervalLabel(startTime, endTime) {
  return `${meridianTime(startTime)} - ${meridianTime(endTime)}`;
}