export const STORAGE_KEY = `api.events`;
export const MIN_TIME_INTERVAL = 30;
export const MINS_IN_DAY = 60 * 24;
export const EVENT_BACKGROUNDS = ['#8C1A6A', '#2E86AB', '#510D0A', '#7D70BA',
  '#F35348', '#68B0AB'];