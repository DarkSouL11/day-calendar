import Vue from 'vue'

import App from './App.vue'
import * as BaseComponents from './components/BaseComponents';
import store from './store';

Vue.config.productionTip = false;

Vue.directive('focus', {
  inserted: function(el, binding) {
    binding.value && el.focus();
  }
});

// Registering base components
for(let x in BaseComponents){
  Vue.component(x, BaseComponents[x]);
}

new Vue({
  render: h => h(App),
  store
}).$mount('#app');
